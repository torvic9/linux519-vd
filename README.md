## vd kernel 5.19 for Arch-compatible desktop systems

- better support for clang
- support for module signing
- multigenerational LRU
- block device LED trigger
- includes recent cpupower
- btrfs patches from -next and lore
- patches from Clear Linux
- patches from linux-hardened
- various fixes, optimisations and backports

See PKGBUILD for source and more details.
